<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'william_powell');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eVdEKuD+2FP:U|Kpl20Ui(?~kFJ],5(/#/4]C<XT)]TXuV~yg]F}VsV~4@A>~WpR');
define('SECURE_AUTH_KEY',  '22G%?rh>`o=sO.dQB;jQ5Fn9HI@r*+JhP3^6]%k?/m%E>W7=$dKlRim;qMOKFkH(');
define('LOGGED_IN_KEY',    'f]BU=AP-J;%C7iGjH;7KI2})`MWf=waaOSVNIjvu.5.T%gLzQ}rZkm9F+-804NrU');
define('NONCE_KEY',        '=1I71*QfhYh^WVf.qj#_LJcrJB-[}Nfn4&$lK@1znmjN<P;#lz=1`b)7J=KS;.CY');
define('AUTH_SALT',        '!+-(:Eo&3NP}GD5+WUq*h=7Y<&os}T$@U;[IXvha|V4az^-0-{-#J)K6UCV<5QA[');
define('SECURE_AUTH_SALT', 'MzA40w$H:H.X+L-:BF611f]i2#?`,S-3?$g9JUueab$aDiuzj;aC~9|*L__t#wH]');
define('LOGGED_IN_SALT',   ';mMe<K7=L/5c$MaA?=p96%V5xP3C6I Vu[0W0e9.)eo|4hNIED0F3A}*/yt-^Y]z');
define('NONCE_SALT',       '%el>[B<!U?A.NGQQw+YY [HR=}vW[w[WmBs>(&f}eCWw$:*b@ w(Ib[DKfT[f@UY');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
