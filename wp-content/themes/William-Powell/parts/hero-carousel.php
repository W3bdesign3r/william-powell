<!-- This is for the main hero carousel -->
<?php if( have_rows('hero_image_carousel') ): ?>
	<div class="owl-carousel owl-theme">
		<?php while( have_rows('hero_image_carousel') ): the_row();
			// vars
			$image         = get_sub_field('hero_image');
			$title         = get_sub_field('hero_title');
			$link          = get_sub_field('hero_link', false, false);
			$link_text     = get_sub_field('hero_button_text');
		?>

			<!-- General slide wrapper -->
			<div class="item">
				<div class="hero_carousel--image"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></div>
				<div class="hero_carousel--overlay show-for-medium">
					<div class="hero_carousel--inner">
						<!-- These are the overlays for each slide -->
						<?php if( $title ): ?><h2 class="hero_carousel--title word-split"><?php echo $title; ?></h2><?php endif; ?>
            <a href="<?php echo get_the_permalink($link); ?>" class="hero_carousel--link"><?php echo $link_text; ?></a>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>
