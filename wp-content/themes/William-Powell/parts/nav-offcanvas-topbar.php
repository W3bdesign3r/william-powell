<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->

<div class="top-bar stickynav" id="top-bar-menu">
	<div class="top-bar show-for-medium">
		<div class="top_bar--inner row">
			<div class="medium-10 columns">
				<?php joints_top_nav(); ?>
			</div>
			<div class="medium-2 right columns">
				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				    <label>
				        <input type="search" class="search-field"
				            placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>"
				            value="<?php echo get_search_query() ?>" name="s"
				            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
				    </label>
				    <input type="submit" class="search-submit" value="<?php echo esc_attr_x( '', 'submit button' ) ?>" /></input>
				</form>
			</div>
		</div>
	</div>

	<div class="top-bar-right float-right show-for-small-only">
		<ul class="menu">
			 <li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
			<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
		</ul>
	</div>
</div>
