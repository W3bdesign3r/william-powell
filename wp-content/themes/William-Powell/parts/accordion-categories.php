<?php // This part is for the categories accordion slider ?>

<?php if( have_rows('woo_tab') ): ?>
	<div class="woo_tab row">
    <!-- tihs sets up the ul and loops over title -->
    <ul class="tabs" data-deep-link="true" data-update-history="true" data-deep-link-smudge="true" data-deep-link-smudge="500" data-tabs id="deeplinked-tabs">
		    <?php while( have_rows('woo_tab') ): the_row();
			    // vars
			    $tab_title         = get_sub_field('tab_title');
          $tab_id            = get_sub_field('tab_id');
		    ?>
          <li class="tabs-title"><a href="#panel<?php echo $tab_id; ?>"><?php echo $tab_title; ?></a></li>
		    <?php endwhile; ?>
    </ul>
    <!-- This loops over the content -->
    <div class="tabs-content" data-tabs-content="deeplinked-tabs">
      <?php while( have_rows('woo_tab') ): the_row();
        // vars
        $tab_content       = get_sub_field('woocommerce_shortcode');
        $tab_id            = get_sub_field('tab_id');
      ?>
        <div class="tabs-panel" id="panel<?php echo $tab_id; ?>">
          <?php echo $tab_content; ?>
        </div>
		 <?php endwhile; ?>
    </div>
	</div>
<?php endif; ?>
