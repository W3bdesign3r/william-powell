<!-- This part is for the main header sectgions above the navigation top bar -->

<?php
	//Vars - These are getting called from main options.
	$tel     = get_field('landline', 'option');
	$logo    = get_field('logo', 'option');
?>

<div class="main_header row">
  <div class="main_header--tel small-10 small-push-1 medium-uncentered medium-4 columns">
    Contact Us: <a href="<?php echo $tel; ?>"><?php echo $tel; ?></a>
  </div>
  <div class="main_header--logo small-10 small-push-1  medium-uncentered medium-4 columns">
    <?php if( !empty($logo) ): ?>
      <img class="logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
    <?php endif; ?>
  </div>
  <div class="main_header--tel small-10 small-pull-1 medium-uncentered medium-4 columns">
    woocommerce stuffs
  </div>
</div>
