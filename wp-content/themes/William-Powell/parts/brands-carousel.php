<!-- This is for the brands  carousel -->
<?php if( have_rows('brands') ): ?>
<div class="brands">
  <div class="brands_carousel--inner row">
  	<div class="brands-carousel owl-theme">
  		<?php while( have_rows('brands') ): the_row();
  			// vars
  			$image   = get_sub_field('brand_logo');
  			$link    = get_sub_field('brand_category_link', false, false);
  		?>
  			<div class="brand-item">
          <a href="<?php echo get_the_permalink($link); ?>" class="brands_carousel--link">
            <div class="brands_carousel--image"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></div>
          </a>
        </div>
  		<?php endwhile; ?>
  	</div>
  </div>
</div>
<?php endif; ?>
