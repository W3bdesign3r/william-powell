
// These are the main controls for the owl carousel on the hero / frontpage.
jQuery(document).ready(function(){
  jQuery('.owl-carousel').owlCarousel({
      loop:true,
      dots: false,
      autoplay:true,
      autoplayHoverPause:false,
      responsive:{
          0:{
              items:1
          },
          600:{
              items:1
          },
          1000:{
              items:1
          }
      }
  })

  //brands carousel
  jQuery('.brands-carousel').owlCarousel({
    loop:true,
    nav: false,
    margin:10,
    dots: false,
    autoplay:true,
    autoplayHoverPause:false,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:4
        },
        1000:{
            items:5
        }
    }
})
});
// This allows for words to be put in spans and split where needed for custom titles.
jQuery(".word-split").lettering('words');
