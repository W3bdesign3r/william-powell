<?php
/*
  This file is part of a child theme called William Powell.
  Functions in this file will be loaded before the parent theme's functions.
  For more information, please read https://codex.wordpress.org/Child_Themes.
*/

//provides woo with acred that the theme is supporting woocommerce
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// Main CSS file.
function william_powell_child_theme_styles() {
  wp_enqueue_style('brick_lime_main_css', get_stylesheet_directory_uri() . '/assets/css/william-powell.css?v='.time(), array(), false, 'all' );
}
add_action('wp_enqueue_scripts','william_powell_child_theme_styles');

//Enqueue additional CSS files here -- Google Fonts / Owl Carousel / Additional fonts
function william_powell_additional_style() {
  wp_enqueue_style( 'google-font-dec', 'https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i', false, 'all'  );
  wp_enqueue_style( 'font-aws', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false, 'all'  );
  wp_enqueue_style('owlcarousel-main', get_stylesheet_directory_uri() . '/assets/css/owl.carousel.min.css');
  wp_enqueue_style('owlcarousel-css-theme-default', get_stylesheet_directory_uri() . '/assets/css/owl.theme.default.min.css');
}
add_action( 'wp_enqueue_scripts', 'william_powell_additional_style' );

function william_powell_scripts() {
	//Standard Owl Carousel libraries getting called in
	wp_register_script('owl_carousel', get_stylesheet_directory_uri() . '/assets/js/owl.carousel.min.js','','', true);
	wp_enqueue_script('owl_carousel');

  //allows Lettering.js to work on certain titles please view controller.js for class to apply to use
  wp_register_script('lettering', get_stylesheet_directory_uri() . '/assets/js/jquery.lettering.js','','', true);
  wp_enqueue_script('lettering');

  //This jQuery file is for all types of jQuery scripts and functions.
	wp_register_script('controller', get_stylesheet_directory_uri() . '/assets/js/controller.js','','', true);
	wp_enqueue_script('controller');

}
add_action( 'wp_enqueue_scripts', 'william_powell_scripts' );

// ACF Options page function -- global business info
if( function_exists('acf_add_options_page') ) { acf_add_options_page('William Powell Settings');}
